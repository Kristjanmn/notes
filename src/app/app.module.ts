import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NoteComponent } from './note/note.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MainComponent } from './main/main.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSliderModule} from "@angular/material/slider";
import {MatCardModule} from "@angular/material/card";
import {MatMenuModule} from "@angular/material/menu";
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from "@angular/material/icon";
import {MatDialogModule, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule, MatLabel} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import { DialogEditNoteComponent } from './dialog-edit-note/dialog-edit-note.component';
import { DialogDeleteNoteComponent } from './dialog-delete-note/dialog-delete-note.component';



const material = [
  MatCardModule,
  MatMenuModule,
  MatButtonModule,
  MatIconModule,
  MatDialogModule,
  MatDialog,
  MatDialogRef,
  MatFormFieldModule,
  MatLabel,
  MatInputModule
];

@NgModule({
  declarations: [
    AppComponent,
    NoteComponent,
    HeaderComponent,
    FooterComponent,
    MainComponent,
    DialogEditNoteComponent,
    DialogDeleteNoteComponent
  ],
  entryComponents: [
    DialogEditNoteComponent,
    DialogDeleteNoteComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatCardModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
