import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.css']
})
export class NoteComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  note: any = {
    title: 'title',
    text: 'text',
    created: 'created date and time'
  };

}
