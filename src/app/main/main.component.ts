import {Component, OnInit} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {DialogEditNoteComponent} from "../dialog-edit-note/dialog-edit-note.component";
import {DialogDeleteNoteComponent} from "../dialog-delete-note/dialog-delete-note.component";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor(public dialog: MatDialog) {}

  //Type notesArrayType = Array<{title: string, context: string}>;
  //notes: string [{title: string}] = []
  public notes:type[] = [];

  ngOnInit(): void {
    this.load()
  }

  editNote(id: number, title: string, content: string): void {
    // Open dialog for editing note. Pretty much same as new note, but has fields filled.
    if(id % 1 === 0 || id % 1 === -0) {
      if(id === -1) {
        // New note
        this.notes.push({title: title, content: content});
      } else {
        // Edit existing note
        this.notes[id] = {title: title, content: content};
      }
    }
    this.save()
  }

  deleteNote(id: number): void {
    if(id !== -1) {
      this.notes.splice(id, 1);
      console.log("Deleted " + id);
    }
    console.log("delete note called");
    this.save();
  }

  openEditDialog(id: number) {
    console.log(id);
    console.log(this.notes);
    let fieldTitle = "New note"
    let title = "";
    let content = "";

    // Check if note exists
    if(id !== -1 && this.notes.length > id && id % 1 == 0) {
      fieldTitle = "Edit note"
      title = this.notes[id].title;
      content = this.notes[id].content;
    }
    let dialogRef = this.dialog.open(DialogEditNoteComponent, {data: {fieldTitle: fieldTitle, title: title, content: content}});
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if(result == 'false') return;
      let note: type = (JSON.parse(result));
      this.editNote(id, note.title, note.content);
    });
  }

  openDeleteDialog(id: number) {
    let dialogRef = this.dialog.open(DialogDeleteNoteComponent);
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`)
      if(result == 'true' && this.notes.length > id) {
        this.deleteNote(id);
        console.log("deleted note " + id);
      }
    });
  }

  /**
   * Save and Load functions for local storage.
   */

  save(): void{
    localStorage.setItem('Notes', JSON.stringify(this.notes))
  }

  load(): void{
    if(localStorage.getItem('Notes')) {
      this.notes = JSON.parse(<string>localStorage.getItem('Notes'))
    } else {
      // Create some placeholder notes.
      this.notes = [
        {title: "Welcome!", content: "Welcome to my notes! :) currently there is nothing here yet"},
        {title: "Another", content: "This is another note. There are many like it, but this one is mine."},
        {title: "Default notes", content: "These notes are here by default. They are added if the 'Notes' key does not exist in local storage"}
      ]
      this.save()
    }
  }

}

export interface type {
  title: string;
  content: string;
}

/*@Component({
  selector: 'noteDeleteDialog',
  templateUrl: 'noteDeleteDialog.html',
})
export class NoteDeleteDialog {}*/
